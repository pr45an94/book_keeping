<?php
include "includes/session.php";
include "includes/dbconfig.php";
include "includes/header.php";
include "account.php";
?>
        <div id="layoutSidenav">
            
            <div id="layoutSidenav_content">
            <?php include "includes/sidebar.php";?>
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Report</h1>
                        <br>
                                             
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">Income</div>                                                
                                    <table style="margin-left:15px; margin-top:5px; padding:5px;">
                                    <?php 
                                        if($count_inc > 0){
                                    ?>    
                                    <thead>
                                            <th>Category</th>
                                            <th>Total Price</th> 
                                            <th>Total Data</th>                                            
                                    </thead>                                                                         
                                    <tbody>
                                    <?php       
                                        $total_inc = 0;                                                                             
                                        mysql_data_seek($run_income, 0);                                                                                
                                        while($row_income = mysql_fetch_array($run_income)):
                                    ?>
                                            <tr>                                                
                                                <td><?php echo incTitle($row_income['cat_id']); ?></td>
                                                <td><?php echo $row_income['total']; ?></td> 
                                                <td><?php echo $row_income['category']; ?></td>                                                
                                            </tr>
                                            <?php
                                            $price = $row_income['category'];
                                            $total_inc += $price;
                                            endwhile;                                            
                                             ?>                                             
                                    </tbody>  
                                    <tr>
                                            <td><Span style="color:green; font-weight:bold;">Grand Total</td> </span>
                                            <td style="color:green; font-weight:bold;"> <?php echo $total_income; ?></td>
                                            <td style="color:green; font-weight:bold;"><?php echo $total_inc; ?></td>
                                    </tr>  
                                    <?php                                            
                                            }else{
                                             ?>
                                             <tr>
                                                 <td>No Record !</td>
                                             </tr>
                                             <?php
                                            }
                                            ?>                                 
                                    </table>
                                    <div class="card-body"><canvas id="" width="100%" height="100%"></canvas>
                                        
                                    </div>                                    
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">Expense</div>
                                    <table style="margin-left:16px; margin-top:5px; padding:5px;">
                                    <?php 
                                        if($count_exp > 0){                                    
                                    ?>  
                                    <thead>
                                            <th>Category</th>
                                            <th>Total Cost</th> 
                                            <th>Total Data</th>                                          
                                    </thead>                                                                           
                                    <tbody>
                                    <?php      
                                        $total = 0;                                       
                                         mysql_data_seek($run_expense, 0);                                                                                   
                                        while($row_expense = mysql_fetch_array($run_expense)):
                                    ?>
                                            <tr>                                                
                                                <td><?php echo expTitle($row_expense['cat_id']); ?></td>
                                                <td><?php echo $row_expense['total']; ?></td>
                                                <td><?php echo $row_expense['category']; ?></td>                                                                                                                                                                                                                                              
                                            <?php
                                            $price = $row_expense['category'];
                                            $total += $price;
                                            endwhile;
                                            
                                            ?>                                                                                                                                     
                                    </tbody>  
                                    <tr>
                                            <td><Span style="color:green; font-weight:bold;">Grand Total</td> </span>
                                            <td style="color:green; font-weight:bold;"> <?php echo $total_expense; ?></td>
                                            <td style="color:green; font-weight:bold;"> <?php echo $total; ?></td>
                                    </tr>  
                                            <?php                                            
                                            }else{
                                             ?>
                                             <tr>
                                                 <td>No Record !</td>
                                             </tr>
                                             <?php
                                            }
                                            ?>                               
                                    </table>                                                                       
                                    <div class="card-body"><canvas id="" width="100%" height="100%"></canvas></div>
                                </div>
                            </div>
                            <div style="margin-left:12px; color:grey; font-weight:bold;">
                               <?php
                                    $diff = 0;                                    
                                    $diff = $total_income - $total_expense;
                                    echo 'Difference : '; 
                                    echo abs($diff);
                                    
                                ?>
                                </div>

                        </div>
                        
                </main>
                <?php include "includes/footer.php"; ?>

