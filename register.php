<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Page Title - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>
                                    <div class="card-body">
                                        <form action="" method="POST">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group"><input class="form-control" name ="first_name" id="inputFirstName" type="text" placeholder="Enter first name" required/></div>
                                                </div>                                                                                        
                                                <div class="col-md-6">
                                                    <div class="form-group"><input class="form-control" name="last_name" id="inputLastName" type="text" placeholder="Enter last name" required/></div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group"><input class="form-control" name="user_pass" id="inputPassword" type="password" placeholder="Enter password" required/></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><input class="form-control" name="user_pass2" id="inputConfirmPassword" type="password" placeholder="Confirm password" required/></div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group"><input class="form-control" name="user_email" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Enter email address" required/></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <select class="form-control" name="user_gender" id="selectGender" required>
                                                            <option disabled>Select Gender</option>
                                                            <option>Male</option>
                                                            <option>Female</option>
                                                            <option>Other</option>
                                                        </select> 
                                                    </div>       
                                                </div>                                                
                                            </div> 
                                            <div class="form-row">                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <select class="form-control" name="user_country" id="selectGender" required="required">
                                                            <option disabled>Select Country</option>
                                                            <option disabled>Select Country</option>
                                                            <option>Nepal</option>
                                                            <option>USA</option>
                                                            <option>China</option>
                                                            <option>UK</option>
                                                            <option>Australia</option>
                                                            <option>India</option>
                                                        </select>  
                                                    </div>  
                                                </div>                                                
                                            </div>                                           
                                            <div class="form-group mt-4 mb-0"><button type="submit" name="create" class="btn btn-primary btn-block">Create Account</button></div>
                                            <?php include "insert_user.php"; ?>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="login.php">Have an account? Go to login</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Nexas Technology 2020</div>
                            <div>                                
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
