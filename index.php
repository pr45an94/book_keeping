<?php
include "includes/session.php";
include "includes/dbconfig.php";
include "includes/header.php";
include "account.php";
?>
        <div id="layoutSidenav">
            
            <div id="layoutSidenav_content">
            <?php include "includes/sidebar.php";?>
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Dashboard</h1>
                        <br>
                        <div class="row">
                        <!--<div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body">Users</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="users.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">Income<span style="margin-left:10px;"><?php echo $total_income; ?> <span> </div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="income.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Expense<span style="margin-left:10px;"><?php echo $total_expense; ?></span> </div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="expense.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Category <span style="margin-left:10px;"><?php echo $count; ?></span></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="category.php">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header"><i class="fas fa-chart-area mr-1"></i>Income Total</div>
                                    <label style="color:green; margin-left:20px; margin-top:10px;" for="">Daily Expense :<label style="margin-left:40px;" for=""></label></label>            
                                    <div class="card-body"><canvas id="" width="100%" height="40"></canvas></div>
                                    
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Expense Total</div>
                                    <label style="color:green; margin-left:20px; margin-top:10px;" for=""><b>Grand Total :<label style="margin-left:10px;" for=""></label></b></label>
                                    <div class="card-body"><canvas id="" width="100%"></canvas></div>
                                </div>
                            </div>
                        </div>-->
                        
                </main>
                <?php include "includes/footer.php"; ?>
