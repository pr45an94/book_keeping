                       <h3>Add / Edit <?php echo ucfirst($type); ?> </h3><br>
                       <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                        <label for="tile"><b>Title:</b></label>
                        <input type="text" class="form-control" name="title" id="" required
                        value="<?php echo isset($editData)?$editData['title']:""; ?>"><br>
                        
                        <label for="description"><b>Description:</b></label>
                        <input type="text" class="form-control" name="description" id="" required
                        value="<?php echo isset($editData)?$editData['description']:""; ?>"><br>
                        
                        <?php
                        if(isset($editData)){
                        ?>
                        <input type="submit" class="btn btn-primary" name="edit" id="edit" value="Edit">
                        <input type="hidden" name="id" value="<?php echo $editData['cat_id']; ?>">
                        <?php
                        }else{
                        ?>
                        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="Add">
                        <?php
                        }
                        ?>
                        <label for=""><?php include "includes/message.php"; ?></label>
                        </form><br>