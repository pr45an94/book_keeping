<div class="card mb-4">                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <?php
                                    if($count > 0){
                                    ?>
                                    <thead>
                                            <tr>
                                                <th>S.N.</th>
                                                <th>Category</th>                                                
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>Price</th>
                                                <th>Delete</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>                                
                                        <tbody>
                                            <?php
                                            $sn = 1;
                                            while($row = mysql_fetch_array($query)):
                                            ?>
                                            <tr>
                                                <td><?php echo $sn++; ?></td>
                                                <td><?php echo catTitle($row['cat_id']); ?></td>                                                
                                                <td><?php echo $row['description']; ?></td>
                                                <td><?php echo $row['date']; ?></td>
                                                <td><?php echo $row['price']; ?></td>
                        
                                                <td>
                                                <a href="?del=<?php echo $row['inc_id']; ?>" class="ico del" onclick="return confirm('Are you sure to delete this record ?')">
                                                 Delete</a></td>
								                <td><a href="?edit=<?php echo $row['inc_id']; ?>" class="ico edit">Edit</a></td>
                                            </tr>

                                            <?php
                                            endwhile;
                                            }else{
                                             ?>
                                             <tr>
                                                 <td colspan="7">No Record !</td>
                                             </tr>
                                             <?php
                                            }
                                            ?>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>