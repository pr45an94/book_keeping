<?php
include "includes/dbconfig.php";
$output = '';

if(isset($_POST['export_income'])){
    $sql = "SELECT * FROM income";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    $sn = 0;
    if($count > 0){
        $output .= '
            <table class="table" bordered="1">
                <tr>
                    <th>S.N.</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Price</th>
                </tr>            
        ';
        while($row = mysql_fetch_array($result)){
            $output .= '
                <tr>
                    <td>'.$sn++.'</td>                    
                    <td>'.catTitle($row["cat_id"]).'</td>
                    <td>'.$row["description"].'</td>
                    <td>'.$row["date"].'</td>
                    <td>'.$row["price"].'</td>
                </tr>
            ';
        }

       $output .= '</table>';
       header("Content-Type: application/xls");
       header("Content-Disposition: attachment; filename=income.xls");
       echo $output;
    }
}

?>