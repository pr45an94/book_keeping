                       <h3>Add / Edit Users </h3><br>
                       <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                        <label for="tile"><b>Fullname:</b></label>
                        <input type="text" class="form-control" name="fullname" id="" required
                        value="<?php echo isset($editData)?$editData['fullname']:""; ?>"><br>
                        <label for="tile"><b>Username:</b></label>
                        <input type="text" class="form-control" name="username" id="" required
                        value="<?php echo isset($editData)?$editData['username']:""; ?>"><br>
                        <label for="cost"><b>Password:</b></label>
                        <input type="password" class="form-control" name="password" id="" required
                        value="<?php echo isset($editData)?$editData['password']:""; ?>"><br>
                        <label for="cost"><b>Email:</b></label>
                        <input type="text" class="form-control" name="email" id="" required
                        value="<?php echo isset($editData)?$editData['email']:""; ?>"><br>
                        <label for=""><b>Status</b></label><br>
                        <select name="status" id="">
                        <?php
                        $active ='selected="selected"';
                        $inactive = '';
                        if(isset($editData)){
                            if(!$editData['status']){
                                $active = '';
                                $inactive ='selected="selected"';
                            }
                        }
                        ?>
                            <option value="1" <?php echo $active; ?>>Active</option>
                            <option value="0" <?php echo $inactive; ?>>In-active</option>
                        </select><br><br>
                        <?php
                        if(isset($editData)){
                        ?>
                        <input type="submit" class="btn btn-primary" name="edit" id="edit" value="Edit">
                        <input type="hidden" name="id" value="<?php echo $editData['id']; ?>">
                        <?php
                        }else{
                        ?>
                        <input type="submit" class="btn btn-primary" name="submit" id="submit" value="Add">
                        <?php
                        }
                        ?>
                        <label for=""><?php include "includes/message.php"; ?></label>
                        </form><br>