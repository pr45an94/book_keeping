<div class="card mb-4">
                            <div class="card-header"><i class="fas fa-table mr-1"></i><?php echo ucfirst($type); ?></div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <?php
                                    if($count > 0){
                                    ?>
                                    <thead>
                                            <tr>
                                                <th>S.N.</th>
                                                <th>Fullname</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Delete</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <!--<tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Office</th>
                                                <th>Age</th>
                                                <th>Start date</th>
                                                <th>Salary</th>
                                            </tr>
                                        </tfoot>-->
                                        <tbody>
                                            <?php
                                            $sn = 1;
                                            while($row = mysql_fetch_array($query)):
                                            ?>
                                            <tr>
                                                <td><?php echo $sn++; ?></td>
                                                <td><?php echo $row['fullname']; ?></td>
                                                <td><?php echo $row['username']; ?></td>
                                                <td><?php echo $row['email']; ?></td>
                                                <td><?php echo $row['postdate']; ?></td>
                                                <td>
                                                <a href="?status=<?php echo $row['id']; ?>" style="color:<?php echo $row['status']?'green':'red'; ?>"
                                                onclick="return confirm('Are you sure to change the status ?')">
                                                <?php echo $row['status']?'Active':'In-active'; ?>
                                                </td>
                                                </a>
                                                <td>
                                                <a href="?del=<?php echo $row['id']; ?>" class="ico del" onclick="return confirm('Are you sure to delete this record ?')">
                                                 Delete</a></td>
								                <td><a href="?edit=<?php echo $row['id']; ?>" class="ico edit">Edit</a></td>
                                            </tr>

                                            <?php
                                            endwhile;
                                            }else{
                                             ?>
                                             <tr>
                                                 <td colspan="7">No Record !</td>
                                             </tr>
                                             <?php
                                            }
                                            ?>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>